from random import randint

with open("lists/word_list.txt", "r") as f:
    words = f.read()
f.closed    

word_list = words.split("\n")
word_list.remove("")

secret_words = []
while len(secret_words) < 20:
    index = randint(0,228)
    secret_word = word_list[index]
    if secret_word not in secret_words:
        secret_words.append(secret_word)

with open("secret_words.txt", "w+") as f:
    for secret_word in secret_words:
        f.write(secret_word + "\n")
f.closed

