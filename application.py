from flask import Flask, render_template, redirect, request, url_for, session
import random
import subprocess
import os

path = os.getcwd()

application = Flask(__name__)

secret_words = []
with open("secret_words.txt", "r") as f:
    raw_secrets = f.readlines()
f.closed

for secret in raw_secrets:
    secret_words.append(secret.strip())

@application.route("/")
def index():
    return render_template("index.html")

@application.route("/listener", methods=["POST"])
def listen():
    secret_said = ""
    response = request.json
    sender = response["name"]
    text = response["text"]

    words = text.split()
    for word in words:
        clean_word = ''.join(ch for ch in word if ch.isalnum())
        clean_word = clean_word.lower()
        if clean_word in secret_words:
            secret_said = clean_word
            break

    if secret_said:
        secret_words.remove(secret_said)
        with open("lists/word_list.txt") as f:
            words = f.readlines()
        f.closed

        index = random.randint(0, 228)
        while words[index].strip() in secret_words:
            index = random.randint(0, 228)

        secret_words.append(words[index].strip())
        with open("secret_words.txt", "w") as f:
            for word in secret_words:
                f.write(word + "\n")
        f.closed

        big_said = secret_said.upper()
        sender = subprocess.Popen(["./secret_said.sh", big_said, sender],
                    stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = sender.communicate()

    return "200 - OK"

@application.route("/secrets")
def messages():
    return render_template("messages.html", messages=secret_words)

if __name__ == '__main__':
    application.run()
